<?php

namespace AzureSpring\Bundle\SiloBundle\DependencyInjection;

use AzureSpring\Silo\ImageVivifier;
use AzureSpring\Silo\Silo;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\Reference;

class AzureSpringSiloExtensionTest extends TestCase
{
    /**
     * @test
     *
     * @throws \Exception
     */
    public function loadWithDefaultConfigurationOK()
    {
        $container = new ContainerBuilder(new ParameterBag([
            'kernel.project_dir' => '/path/to/project',
        ]));
        $container->registerExtension(new AzureSpringSiloExtension());
        $container->loadFromExtension('azurespring_silo');
        $container
            ->register(ServiceConsumer::class)
            ->addArgument(new Reference(Silo::class))
            ->setPublic(true)
        ;
        $container->getCompilerPassConfig()->setOptimizationPasses([]);
        $container->getCompilerPassConfig()->setRemovingPasses([]);
        $container->compile();

        $this->assertEquals(
            new Silo('/path/to/project/public/fs', [new ImageVivifier()]),
            $container->get(ServiceConsumer::class)->getService()
        );
    }

    /**
     * @test
     *
     * @throws \Exception
     */
    public function loadWithoutVivifierOK()
    {
        $container = new ContainerBuilder(new ParameterBag([
            'kernel.project_dir' => '/path/to/project',
        ]));
        $container->registerExtension(new AzureSpringSiloExtension());
        $container->loadFromExtension('azurespring_silo', [
            'vivifiers' => null,
        ]);
        $container
            ->register(ServiceConsumer::class)
            ->addArgument(new Reference(Silo::class))
            ->setPublic(true)
        ;
        $container->getCompilerPassConfig()->setOptimizationPasses([]);
        $container->getCompilerPassConfig()->setRemovingPasses([]);
        $container->compile();

        $this->assertEquals(
            new Silo('/path/to/project/public/fs'),
            $container->get(ServiceConsumer::class)->getService()
        );
    }
}
