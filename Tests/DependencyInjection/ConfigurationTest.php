<?php

namespace AzureSpring\Bundle\SiloBundle\DependencyInjection;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Definition\Processor;

class ConfigurationTest extends TestCase
{
    /**
     * @test
     */
    public function processDefaultConfigurationOK()
    {
        $processor = new Processor();
        $config = $processor->processConfiguration(new Configuration(), []);

        $this->assertEquals(
            [
                'root_dir'  => '%kernel.project_dir%/public/fs',
                'input'     => 'file',
                'vivifiers' => [
                    'image',
                ],
            ],
            $config
        );
    }

    /**
     * @test
     */
    public function processNoneVivifierOK()
    {
        $processor = new Processor();
        $config = $processor->processConfiguration(new Configuration(), [['vivifiers' => null]]);

        $this->assertEquals(
            [
                'root_dir'  => '%kernel.project_dir%/public/fs',
                'input'     => 'file',
                'vivifiers' => [],
            ],
            $config
        );
    }
}
