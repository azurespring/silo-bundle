<?php

namespace AzureSpring\Bundle\SiloBundle\DependencyInjection;

/**
 * ServiceConsumer
 */
class ServiceConsumer
{
    private $service;

    /**
     * @param $service
     */
    public function __construct($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }
}
