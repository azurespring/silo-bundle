<?php

namespace AzureSpring\Bundle\SiloBundle;

use AzureSpring\Bundle\SiloBundle\Controller\DefaultController;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\XmlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

class ResourcesTest extends TestCase
{
    /**
     * @test
     */
    public function loadRoutesOK()
    {
        $loader = new XmlFileLoader(new FileLocator([__DIR__.'/../Resources/config']));
        $routes = $loader->load('routes.xml');
        $this->assertInstanceOf(RouteCollection::class, $routes);

        $matcher = new UrlMatcher($routes, new RequestContext('/'));
        $this->assertEquals(
            [
                '_controller' => DefaultController::class.'::getAction',
                'path'        => '2a/51/2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg',
                '_route'      => 'azurespring_silo_get',
            ],
            $matcher->match('/2a/51/2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg')
        );
        $this->assertEquals(
            [
                '_controller' => DefaultController::class.'::getAction',
                '_route'      => 'azurespring_silo_get',
                'path'        => 'crop/400,400/2a/51/2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg',
            ],
            $matcher->match('/crop/400,400/2a/51/2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg')
        );

        $matcher = new UrlMatcher($routes, new RequestContext('/', 'POST'));
        $this->assertEquals(
            [
                '_controller' => DefaultController::class.'::postAction',
                '_route'      => 'azurespring_silo_post',
            ],
            $matcher->match('/')
        );
    }
}
