<?php

namespace AzureSpring\Bundle\SiloBundle\Tests\Controller;

use AzureSpring\Bundle\SiloBundle\Controller\DefaultController;
use AzureSpring\Silo\SiloInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultControllerTest extends TestCase
{
    /**
     * @test
     *
     * @throws \ReflectionException
     */
    public function getOK()
    {
        $silo = $this->createMock(SiloInterface::class);
        $silo
            ->expects($this->once())
            ->method('find')
            ->with($this->equalTo(['crop', '400,400', '2a', '51', '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg']), $this->isFalse())
            ->willReturn(new File(__DIR__.'/../DSC05448.jpg'))
        ;

        $controller = new DefaultController($silo, 'file');

        $this->assertEquals(
            new BinaryFileResponse(new File(__DIR__.'/../DSC05448.jpg')),
            $controller->getAction('crop/400,400/2a/51/2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg')
        );
    }

    /**
     * @test
     */
    public function postOK()
    {
        $silo = $this->createMock(SiloInterface::class);
        $silo
            ->method('save')
            ->with($this->equalTo(new UploadedFile(__DIR__.'/../DSC05448.jpg', 'DSC05448.jpg')))
            ->willReturn('2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg')
        ;
        $request = Request::create('whatever', 'POST', [], [], [
            'file' => [
                new UploadedFile(__DIR__.'/../DSC05448.jpg', 'DSC05448.jpg'),
            ],
        ]);

        $controller = new DefaultController($silo, 'file');

        $this->assertEquals(new JsonResponse(['filenames' => ['2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg']]), $controller->postAction($request));
    }
}
