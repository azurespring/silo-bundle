<?php
/*
 * Configuration.php
 */
namespace AzureSpring\Bundle\SiloBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Configuration
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $class = new \ReflectionClass(TreeBuilder::class);
        if ($class->hasMethod('getRootNode')) {
            $treeBuilder = new TreeBuilder('azurespring_silo');
            $rootNode = $treeBuilder->getRootNode();
        } else {
            $treeBuilder = new TreeBuilder();
            $rootNode = $treeBuilder->root('azurespring_silo');
        }

        $rootNode
            ->children()
                ->scalarNode('root_dir')
                    ->cannotBeEmpty()
                    ->defaultValue('%kernel.project_dir%/public/fs')
                ->end()
                ->scalarNode('input')
                    ->cannotBeEmpty()
                    ->defaultValue('file')
                ->end()
                ->arrayNode('vivifiers')
                    ->defaultValue(['image'])
                    ->scalarPrototype()->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
