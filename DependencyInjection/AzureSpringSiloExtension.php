<?php
/*
 * AzureSpringSiloExtension.php
 */
namespace AzureSpring\Bundle\SiloBundle\DependencyInjection;

use AzureSpring\Silo\ImageVivifier;
use AzureSpring\Silo\Silo;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

/**
 * AzureSpringSiloExtension
 */
class AzureSpringSiloExtension extends Extension
{
    /**
     * @inheritDoc
     *
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('azurespring_silo.root_dir', $config['root_dir']);
        $container->setParameter('azurespring_silo.input', $config['input']);

        $shortcuts = ['image' => ImageVivifier::class];
        $silo = $container->getDefinition(Silo::class);
        $vivifiers = [];
        foreach ($config['vivifiers'] as $v) {
            $vivifiers[] = new Reference(@$shortcuts[$v] ?? $v);
        }
        $silo->replaceArgument('$vivifiers', $vivifiers);
    }

    /**
     * @inheritDoc
     */
    public function getAlias()
    {
        return 'azurespring_silo';
    }
}
