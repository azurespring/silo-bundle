<?php
/*
 * AzureSpringSiloBundle.php
 */
namespace AzureSpring\Bundle\SiloBundle;

use AzureSpring\Bundle\SiloBundle\DependencyInjection\AzureSpringSiloExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * AzureSpringSiloBundle
 */
class AzureSpringSiloBundle extends Bundle
{
    /**
     * @inheritDoc
     */
    public function getContainerExtension()
    {
        return new AzureSpringSiloExtension();
    }
}
