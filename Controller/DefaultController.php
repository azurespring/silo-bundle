<?php
/*
 * DefaultController.php
 */
namespace AzureSpring\Bundle\SiloBundle\Controller;

use AzureSpring\Silo\SiloInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * DefaultController
 */
class DefaultController
{
    private $silo;

    private $input;

    /**
     * @param SiloInterface $silo
     * @param string        $input
     */
    public function __construct(SiloInterface $silo, string $input)
    {
        $this->silo = $silo;
        $this->input = $input;
    }

    /**
     * @param string $path
     *
     * @return BinaryFileResponse
     */
    public function getAction(string $path)
    {
        return new BinaryFileResponse($this->silo->find(explode('/', $path)));
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function postAction(Request $request)
    {
        return new JsonResponse([
            'filenames' => array_map(
                function (UploadedFile $file) {
                    return $this->silo->save($file);
                },
                $request->files->get($this->input)
            ),
        ]);
    }
}
